import React from 'react';
import styled from 'styled-components';
import './App.css';

const Background = styled.div`
  width: calc(100vw);
  height: calc(100vh);
  background: #594963;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const BigText = styled.div`
  font-family: 'Lucida Console', 'Courier New';
  text-transform: uppercase;
  font-size: 200px;
  color: #f0f5f1;
`;

function App() {
  return (
    <div className="App">
      <Background>
        <BigText>Pipeline example!</BigText>
      </Background>
    </div>
  );
}

export default App;
