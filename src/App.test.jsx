import { render, screen } from '@testing-library/react';
import App from './App';

describe('Test set 1', () => {
  test('Example test', () => {
    render(<App />);
    const mainBody = screen.getByText(/Pipeline example!/i);
    expect(mainBody).toBeInTheDocument();
  });
});
