# Ci/CD Pipeline intergration example

### The repo

The application is just a simple react application built from create-react-app, set up with eslint and prettier. This is just a simple demonstration of how a CI/CD pipeline can be implemented into your application.

### CI/CD Pipeline

[CircleCI](https://circleci.com/) is used to create and run the workflow tests. 

On each commit, the test will run:

Check for eslint errors
Check for prettier errors
Run all tests